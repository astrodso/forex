<?php

namespace app\models;

use yii\base\Model;
use Yii;

/**
 * Модель работы с файлом
 *
 * @author Max
 */
class File extends Model
{
    public $file;
    
    public $_dom;
    public $_data;
    public $_xpath;
    public $_result;

    //разрешенные типы
    const TYPE = [
        'balance',
        'buy',
        'sell'
    ];

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'html'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->file->saveAs('uploads/'.$this->file->baseName.'.'.$this->file->extension);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Извлекает данные из файла
     * @return $this
     */
    protected function getData(){
        $result = [
            'series' => [
                'data' => []
            ],
            'categories' => [
                'data' => []
            ]
        ];

        $table = $this->_xpath->query('//table')->item(0);
        $rows  = $table->getElementsByTagName("tr");

        $balance = 0;

        foreach ($rows as $row) {
            $childNodes = $row->childNodes;
            //если нужный тип данных
            if ( isset($childNodes->item(2)->nodeValue) && in_array( $childNodes->item(2)->nodeValue, File::TYPE )) {

                $itemBalance = doubleval( preg_replace('|\s+|', '', $row->lastChild->nodeValue ) );

                $balance = $balance + $itemBalance;

                $result['series']['data'][]     = $balance;

                $result['categories']['data'][] = $childNodes->item(1)->nodeValue;

            }
        }
        $this->_result = $result;

        return $this;
    }

    /**
     * Возвращает результирующий массив
     * @return array()
     */
    public function parseFile()
    {
        $this->getFile()
            ->createDomDocument()
            ->createDomXpath()
            ->getData();

        return $this->_result;
        
    }

    /**
     * Загружает файл
     * @return $this
     */
    protected function getFile()
    {
        $this->_data = file_get_contents('uploads/'.$this->file->baseName.'.'.$this->file->extension);
        return $this;
    }

    /**
     * Создает DomDocument
     * @return $this
     */
    protected function createDomDocument()
    {
        $this->_dom = new \DOMDocument();
        libxml_use_internal_errors(true);
        if ($this->_dom->loadHTML($this->_data)) {
            Yii::info(Yii::t('app', 'Create DomDocument'));
        } else {
            Yii::info(Yii::t('app',
                    'An error occurred when creating an object of class DOMDocument'));
        }
        libxml_use_internal_errors(false);

        return $this;
    }

    /**
     * Создает DomXpath
     * @return $this
     */
    protected function createDomXpath()
    {
        $this->_xpath = new \DOMXPath($this->_dom);

        Yii::info(Yii::t('app', 'Create DomXpath'));

        return $this;
    }
}