<?php
use skeeks\widget\highcharts\Highcharts;
use kartik\file\FileInput;
use yii\bootstrap\ActiveForm;
?>
<?php $this->title = 'Forex';?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'form-inline']])?>

    <?= $form->field($model, 'file')->widget(FileInput::classname(), [
            'pluginOptions' => [
                'showPreview' => false,
                'showUpload' => true,
            ],
            'options' => [
                'multiple' => false
            ]
        ]);
    ?>

<?php ActiveForm::end() ?>
<?php if (isset($data['series']['data']) && !empty($data['series']['data'])): ?>
    <?= Highcharts::widget([
        'options' => [
            'chart'  => [
                'zoomType' => 'x'
            ],
            'title'  => ['text' => 'Profit'],
            'xAxis'  => [
                'type'       => 'datetime',
                'categories' => $data['categories']['data'],
                'labels' => [
                    'enabled' => false,
                ]
            ],
            'yAxis'  => [
                'title' => ['text' => 'Balance']
            ],
            'series' => [
                [
                    'type' => 'area',
                    'name' => 'Balance',
                    'data' => $data['series']['data']
                ]
            ]
        ]
    ]);
    ?>
<?php elseif( isset( $data['series']['data'] ) && empty($data['series']['data']) ): ?>
    <h2><?= Yii::t("app", "Данные отсутствуют")?></h2>
<?php endif; ?>
